package observadores;

import java.util.ArrayList;

import simuladorSensores.DatoSensor;
import sujetos.ReceptorInfoSensores;

public class HUMEDADOperario extends OperarioGranja{
	
	public HUMEDADOperario(ArrayList<ReceptorInfoSensores> receptores,String nombre) {
		super(receptores);
		super.nombre=nombre;
	}
	protected boolean interesado() {
		return true;
	}
	protected void actuar(DatoSensor dato) {
		System.out.println(super.nombre+" resuelve problemas con sensor de HUMEDAD");
	}
	public String getNombre() {
		return this.nombre;
	}
}
	