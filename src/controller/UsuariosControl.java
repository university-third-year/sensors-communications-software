package controller;

import java.util.HashMap;

import javax.swing.JOptionPane;

import common.Usuario;
import model.UsuarioModel;
import view.RegistroUsuario;

public class UsuariosControl {

	private HashMap<String,Usuario> UsuariosBD;
	public RegistroUsuario view;
	public UsuarioModel model;
	String a = "a";

	public UsuariosControl() {
	}
	
	public UsuariosControl (RegistroUsuario view,UsuarioModel model) {
		this.view = view;
		this.model = model; 
		initUsuarioControl();

	}

	private void initUsuarioControl() {
		view.btnContinuar.addActionListener(e->aniadeInfo());
	}


	@SuppressWarnings("unlikely-arg-type")
	private void aniadeInfo() {

		model.recoverBD();
		String loginName= view.textFieldUsuario.getText();
		//System.out.println("Nombre: "+ loginName);
		@SuppressWarnings("deprecation")
		String password =  view.passwordField.getText();
		//System.out.println("Contra: "+ password);
		String type = view.textFieldRol.getText();
		//System.out.println("Tipo:   "+ type);
		Usuario usuario = new Usuario(loginName, password,type.charAt(0));

		if(password.length() > 8) {
			JOptionPane.showMessageDialog(null, "contrase�a de m�s de 8 caracteres");
			common.GlobalInstances.frameRegistroUsuario.setVisible(false);
			common.GlobalInstances.frameBienvenida.setVisible(true);

		}else {
			if(model.buscarUsuario(loginName) == true) {
				common.GlobalInstances.frameRegistroUsuario.setVisible(false);
				common.GlobalInstances.frameBienvenida.setVisible(true);
				//este nombre ya esta utilizado
				JOptionPane.showMessageDialog(null, "este nombre ya esta utilizado");

			}else {
				if(type.equals(a)) {
					// administrador
					System.out.println("pilla la 'a'");
					System.out.println(model.contarAdmin());
					if(model.contarAdmin() == true) {
						System.out.println("ya hay 3 admins, llega");
						common.GlobalInstances.frameRegistroUsuario.setVisible(false);
						common.GlobalInstances.frameBienvenida.setVisible(true);
						//ya hay 3 administradores
						JOptionPane.showMessageDialog(null, "ya hay 3 administradores");
					}else {
						model.addUsuario(usuario);
						model.dump();

						common.GlobalInstances.frameRegistroUsuario.setVisible(false);
						common.GlobalInstances.frameInterfazLogin.setVisible(true);
						JOptionPane.showMessageDialog(null, "el usuario se ha a�adido correctamente");
					}
				}else {
					// operario
					model.addUsuario(usuario);
					model.dump();

					common.GlobalInstances.frameRegistroUsuario.setVisible(false);
					common.GlobalInstances.frameInterfazLogin.setVisible(true);
					JOptionPane.showMessageDialog(null, "el usuario se ha a�adido correctamente");
				}
			}
		}
	}
}
