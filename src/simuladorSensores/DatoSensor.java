package simuladorSensores;

import java.text.DecimalFormat;
import java.util.GregorianCalendar;

/**
 * @author: Mar�a Blanca Ib��ez
 * 
 * CLASE: DatoSensor (Clase que gestiona la medici�n de un sensor) 
 * 
 * FUNCIONALIDAD:
 *       (1) Gestiona la informaci�n inherente a una medida producida por un sensor: 
 *       		valor de la medida: valor; momento en el que se produjo la medida: fecha
 *       		precondici�n: el valor de la medida es v�lida
 *       
 *       (2) Permite al desarrollador obtener informaci�n acerca de la medici�n (toString())
 *       
 */

public class DatoSensor {
	private String            nombre;
	
	private double            valor;
	private GregorianCalendar fecha;
	
	/**
	 * Cada dato que genera el sensor tiene: nombre del sensor, la medida generada por el sensor, la fecha en la que se gener� la medida
	 * @param nombre: nombre del sensor
	 * @param valor: medida generada por el sensor
	 */
	public DatoSensor(String nombre, double valor){
		this.nombre = nombre;
		this.valor  = valor;
		this.fecha  = new GregorianCalendar();
	}
	
	public String getNombre(){
		return nombre;
	}
	
	public double getValor(){
		return valor;
	}

	public String toString(){
		DecimalFormat df = new DecimalFormat("0.0000");	
		return String.format("%-25s", nombre) + " " 
				+ String.format("%10s",df.format(valor)) + "  " 
				+ fecha.getTime();
	}
}
