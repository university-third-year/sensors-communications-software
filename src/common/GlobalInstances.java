package common;


public class GlobalInstances {

	public static view.MenuOperador frameMenuOperador;
	public static view.Interfazlogin frameInterfazLogin;
	public static view.MenuAdministrador frameMenuAdministrador;
	public static view.Bienvenida frameBienvenida;	
	public static view.RegistroUsuario frameRegistroUsuario;
	public static view.confirmPopUp frameConfirmPopUp;
    public static view.Tipo frameTipo;
    public static view.Identificador frameIdentificador;
    public static view.InterfazCese frameInterfazCese;
    public static view.InscripcionSensor frameInscripcionSensor;
    public static view.DesinscripcionSensor frameDesinscripcionSensor;


	private static GlobalInstances INSTANCE= null;

	private GlobalInstances() {}

	private static void createInstance() {
		if (INSTANCE == null) {
			INSTANCE = new GlobalInstances();
		}
	}
	public static GlobalInstances getInstance() {
		if (INSTANCE == null) createInstance();
		return INSTANCE;
	}

}


