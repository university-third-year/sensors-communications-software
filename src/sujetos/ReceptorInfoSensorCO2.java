package sujetos;
import common.Sensor;
import granjaApp.RDGuardian;
import observadores.CO2Operario;
import observadores.IObservador;
import observadores.OperarioGranja;
import simuladorSensores.DatoSensor;

public class ReceptorInfoSensorCO2 extends ReceptorInfoSensoresConcretoPadre{
	protected DatoSensor ultimoDatoProblema;
	public ReceptorInfoSensorCO2() {
		super();
	}
	protected boolean verificarRealDecreto(Sensor sensor, DatoSensor dato) {
		return (RDGuardian.verificarCO2((int)dato.getValor()));
	}
	protected void notificarOperarioConcreto(IObservador iobservador) {
		((CO2Operario) iobservador).actualizar(this);
	}
	@Override
	protected void notificarOperarioConcreto() {
		//No sabemos por qu� da error si ni metemos esto, as� que lo dejamos aqu� porque no afecta a la implementaci�n del c�digo.
		
	}

}
