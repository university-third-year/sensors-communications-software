package common;


import java.io.FileNotFoundException;
import java.io.IOException;

import common.Coordenadas;
import granjaApp.Granja;
import simuladorSensores.estrategiasGeneracionDatos.GeneracionLectura;
import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;


public class Sensor {
	protected String			tipo;
	protected String            nombre;
	protected int         		idSensor;
	protected double            minValor; 
	protected double            maxValor;
	protected Coordenadas       coordenadas;
	protected GeneradorDatos    generacionDatos;
	protected Granja  		    granja;

	private static int          id             = 1;
	private static int          idCO2          = 1;
	private static int          idNH3          = 1;
	private static int          idHUM          = 1;
	private static int          idTMPEXT       = 1;
	private static int          idTMPINT       = 1;

	public Sensor(String tipo, Coordenadas coordenadas, double minValor, double maxValor, GeneradorDatos generacionDatos,  Granja granja) throws FileNotFoundException, IOException{ //, GeneradorDatos generacionDatos,  Granja granja
		
		if(tipo.equals("CO2")){
			this.idSensor = idCO2;
			this.nombre = tipo + "_" + idCO2;
			System.out.println("idCO2: " + idCO2);
			idCO2++;
		}
		if(tipo.equals("HUMEDAD")){
			this.idSensor = idHUM;
			this.nombre = tipo + "_" + idHUM;
			System.out.println("idHUM: " + idHUM);
			idHUM++;
		}
		if(tipo.equals("NH3")){
			this.idSensor = idNH3;
			this.nombre = tipo + "_" + idNH3;
			System.out.println("idNH3: " + idNH3);
			idNH3++;
		}
		if(tipo.equals("TEMP_EXTERNA")){
			this.idSensor = idTMPEXT;
			this.nombre = tipo + "_" + idTMPEXT;
			System.out.println("idTMPEXT: " + idTMPEXT);
			idTMPEXT++;
		}
		if(tipo.equals("TEMP_INTERNA")){
			this.idSensor = idTMPINT;
			this.nombre = tipo + "_" + idTMPINT;
			System.out.println("idTMPINT: " + idTMPINT);
			idTMPINT++;
		}
		
		//id = idSensor;
		
		this.coordenadas = coordenadas;
		this.tipo = tipo;
		this.minValor = minValor;
		this.maxValor = maxValor;
		if(generacionDatos!=null) {
		this.generacionDatos=generacionDatos;
		}else {
			this.generacionDatos= nuevoGenerador(this.nombre);
		}
		this.granja=granja;
		this.generacionDatos.observarIntervalo(this.nombre, this.minValor, this.maxValor);
	}
	private GeneradorDatos nuevoGenerador (String nombre) throws FileNotFoundException, IOException {
		String file="valores_"+nombre+".txt";
		GeneradorDatos gen= new GeneracionLectura(file);
		return gen;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipoSensor() {
		return this.tipo; 
	}

	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}

	public void setId(int id) {
		Sensor.id = id++;
	}

	public String getNombre() {
		return nombre;
	}

	public int getId() {
		return this.idSensor;
	}

	public Coordenadas getCoordenadas() {
		return coordenadas;
	}
/*
	public void run(){		
		for (int i = 0; sensorVivo && (i < numIteraciones); i++){
			granja.recibirDato(this, generacionDatos.generarNuevaMedida());		
			try {
				Thread.sleep(tiempoDeEspera); // El pr�ximo valor lo genera en 20 milisegundos
			} catch (InterruptedException e) { }
		}
	}

	public void matarSensor() {
		sensorVivo = false;
	}
*/
	public String toString(){
		return String.format("%-25s",nombre) + 
				" capaz de medir de: " + 
				String.format("%10.4f",minValor) + " a:" + String.format("%10.4f",maxValor);
	}

}