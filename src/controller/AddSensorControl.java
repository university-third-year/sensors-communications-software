package controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JOptionPane;

import common.Coordenadas;
import granjaApp.Granja;
import model.SensorModel;
import simuladorSensores.estrategiasGeneracionDatos.GeneracionLectura;
import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;
import view.MenuAdministrador;
import view.Tipo;

public class AddSensorControl {

	MenuAdministrador menuAdministrador;
	Tipo tipo;
	SensorModel model;
	Granja granja;

	public AddSensorControl( SensorModel model, Tipo tipo, Granja granja) {
		this.tipo = tipo;
		this.model = model;
		this.granja=granja;
		initAddSensorControl();
	}
	public AddSensorControl() {
	}
	
	private void initAddSensorControl() {
		tipo.btnTipo.addActionListener(e ->{
			try {
				AniadirSens();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
	}

	public void AniadirSens() throws FileNotFoundException, IOException {

		//leer fichero y almacenar todo en HashMap
		//model.recover_sensBD(this.granja);
		
		//variables de las ventanas (window)
		String tipoW = tipo.tipo_sensor.getText();
		int xW = Integer.parseInt(tipo.coorX.getText());
		int yW = Integer.parseInt(tipo.coorY.getText());
		
		/*String file = "valores_"+tipoW+"_"+id+".txt";
		GeneradorDatos gen = new GeneracionLectura(file);*/
		//revisar condiciones
		if  ( model.AddConditions(tipoW, xW, yW) == true) {
			
			//a�adimos el sensor en HashMap
			model.aniadirSensor(tipoW, new Coordenadas(xW,yW),null,this.granja);
			
			//volcar el HashMap al fichero
			model.dump_sens();

			//mensaje de confirmacion
			JOptionPane.showMessageDialog(null, "Sensor con tipo: "+tipoW+
					"\nY coordenadas: ("+xW+", "+yW+")\n"+
					"fue a�adido correctamente");
		}else {
			//mensaje de error
			JOptionPane.showMessageDialog(null, "Error al a�adir sensor con tipo: "+tipoW+
					"\nY coordenadas: ("+xW+", "+yW+")");
		}	
	}
}


