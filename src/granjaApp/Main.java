/*Anotaci�n: en la funcion inscribir de ReceptorInfoSensores, nos da siempre NullPointerException, y hemos comprobado de que ning�n par�metro que pasamos es nulo.
 Hemos podido observar que este error lo arrastramos desde la pr�ctica de patr�n observador. Por lo tanto, al darnos cuenta de este error, y no saber solucionarlos, ni siquiera despu�s
 de algunas tutor�as, hemos decidido seguir con el c�digo a pesar de los errores. Por favor, pedimos que tengan esto en cuenta.
 Atentamente:
 Victor Santos L�pez
 Tom�s S�nchez de Dios
 Adri�n Francisco Brenes Mart�nez*/

package granjaApp;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import view.*;
import common.GlobalInstances;
import controller.AddSensorControl;
import controller.CeseControl;
import controller.DesinscribirControl;
import controller.ElimSensorControl;
import controller.InscribirControl;
import controller.LogOutControl;
import controller.LoginControl;
import controller.UsuariosControl;
import model.SensorModel;
import model.UsuarioModel;
import sujetos.Receptor;
import sujetos.ReceptorInfoSensorCO2;
import sujetos.ReceptorInfoSensorNH3;
import sujetos.ReceptorInfoSensorTEMP_EXT;
import sujetos.ReceptorInfoSensorTEMP_INT;
import view.Interfazlogin;
import  view.MenuAdministrador;
import view.MenuOperador;
import view.RegistroUsuario;
import view.Tipo;
import view.confirmPopUp;
import view.Bienvenida;
import view.Identificador;
import view.InterfazCese;

public class Main extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//Inicializamos las interfaces
					Interfazlogin view= new Interfazlogin();
					MenuAdministrador admin=new MenuAdministrador();
					MenuOperador operador=new MenuOperador();
					Bienvenida bienv =new Bienvenida();
					confirmPopUp confirmpopup = new confirmPopUp();
					RegistroUsuario viewRegistroPantalla= new RegistroUsuario();
					Tipo frameTipo = new Tipo();
					Identificador frameIdentificador = new Identificador();
					InterfazCese frameInterfazCese = new InterfazCese();
					InscripcionSensor frameInscripcionSensor= new InscripcionSensor();
					DesinscripcionSensor frameDesinscripcionSensor=new DesinscripcionSensor();
					//inicializamos objetos que ser�n necesarios en la simulaci�n
					Granja granja=new Granja();
					Receptor receptor=new Receptor();
					
					granja.setReceptorInfoSensorNH3(receptor.recepNH3);
					granja.setReceptorInfoSensorCO2(receptor.recepCO2);
					granja.setReceptorInfoSensorHUMEDAD(receptor.recephumedad);
					granja.setReceptorInfoSensorTEMP_EXT(receptor.recepTEMPEXT);
					granja.setReceptorInfoSensorTEMP_INT(receptor.recepTEMPINT);
					
					UsuarioModel model= new UsuarioModel();
					SensorModel model_ = new SensorModel();
					
					GlobalInstances.frameInterfazLogin=view;
					GlobalInstances.frameMenuOperador=operador;
					GlobalInstances.frameMenuAdministrador=admin;
					GlobalInstances.frameBienvenida=bienv;
					GlobalInstances.frameConfirmPopUp=confirmpopup;
					GlobalInstances.frameRegistroUsuario = viewRegistroPantalla;
					GlobalInstances.frameTipo = frameTipo;
					GlobalInstances.frameIdentificador = frameIdentificador;
					GlobalInstances.frameInterfazCese = frameInterfazCese;
					GlobalInstances.frameInscripcionSensor=frameInscripcionSensor;
					GlobalInstances.frameDesinscripcionSensor=frameDesinscripcionSensor;
					
					bienv.setVisible(true);
					view.setVisible(false);
					operador.setVisible(false);
					admin.setVisible(false);
					viewRegistroPantalla.setVisible(false);
					confirmpopup.setVisible(false);
					frameTipo.setVisible(false);
					frameIdentificador.setVisible(false);
					frameInterfazCese.setVisible(false);
					frameInscripcionSensor.setVisible(false);
					frameDesinscripcionSensor.setVisible(false);
					
					LoginControl control = new LoginControl(view, model,model_,granja,receptor);
					UsuariosControl controlRegistro = new UsuariosControl(viewRegistroPantalla, model);
					CeseControl controlCese = new CeseControl(frameInterfazCese, model,granja);
					LogOutControl controlLogout = new LogOutControl(confirmpopup,model,granja);
					AddSensorControl addControl = new AddSensorControl(model_, frameTipo,granja);
					ElimSensorControl elimControl = new ElimSensorControl(model_, frameIdentificador,granja);
					InscribirControl inscricontrol= new InscribirControl(model_,model,frameInscripcionSensor,granja,receptor);
					DesinscribirControl desinscribirControl= new DesinscribirControl(model_,model,frameDesinscripcionSensor,granja);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

