package observadores;
import java.util.ArrayList;
import simuladorSensores.DatoSensor;
import sujetos.ReceptorInfoSensores;

public abstract class OperarioGranja implements IObservador{
	protected String nombre;
	protected ArrayList<ReceptorInfoSensores> servicios;
	
	OperarioGranja(ArrayList<ReceptorInfoSensores> servicios) {
		this.servicios=servicios;
		//cada vez que creamos un OperarioGranja, este se suscribe a todos los ReceptoeInfoSensores del ArrayList que se le pasa en los argumentos.
		for(int i=0;i<servicios.size();i++) {
			ReceptorInfoSensores servicio =(ReceptorInfoSensores)servicios.get(i);
			servicio.inscribir(this);
		}
	}
	
	public void actualizar(ReceptorInfoSensores receptor) {
		if(interesado()) {
			DatoSensor dato=receptor.getDatoSensor();
			actuar(dato);
		}
	}
	protected abstract void actuar(DatoSensor dato);
	protected abstract boolean interesado();
}
