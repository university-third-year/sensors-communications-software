package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import common.GlobalInstances;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

public class MenuOperador extends JFrame {
	/**
	 * Create the frame.
	 */
	public MenuOperador() {
		setTitle("Men\u00FA Operador\r\n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GlobalInstances.frameConfirmPopUp.setVisible(true);
				
			}
		});
		
		JButton btnInscribirseASensor = new JButton("Inscribirse a sensor");
		btnInscribirseASensor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GlobalInstances.frameMenuOperador.setVisible(false);
				GlobalInstances.frameInscripcionSensor.setVisible(true);
				
			}
		});
		
		JButton btnDesinscribirseDeSensor = new JButton("Desinscribirse de sensor");
		btnDesinscribirseDeSensor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GlobalInstances.frameMenuOperador.setVisible(false);
				GlobalInstances.frameDesinscripcionSensor.setVisible(true);
				
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(57)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnDesinscribirseDeSensor)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnInscribirseASensor)
							.addPreferredGap(ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
							.addComponent(btnLogout, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
							.addGap(79))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(59)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnLogout, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnInscribirseASensor))
					.addPreferredGap(ComponentPlacement.RELATED, 77, Short.MAX_VALUE)
					.addComponent(btnDesinscribirseDeSensor)
					.addGap(66))
		);
		getContentPane().setLayout(groupLayout);
	}
}
