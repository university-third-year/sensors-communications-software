package simuladorSensores.estrategiasGeneracionDatos;

import simuladorSensores.DatoSensor;

/**
 * @author: Mar�a Blanca Ib��ez
 * 
 * CLASE: GeneradorDatos (Clase base abstracta, de ella derivan clases que representan estrategias 
 * 			de generacion de datos tales como Random, Lectura a partir de un fichero) 
 * 
 * FUNCIONALIDAD:
 *       (1) Genera nueva medida y la empaqueta como un Dato seg�n la estrategia que representa: 
 *       		public abstract DatoSensor generarNuevaMedida().
 *       (2) Permite verificar si la medida generada con la estrategia que representa est� dentro de los rangos
 *       		que puede generar el sensor del que forma parte.
 *       
 */

public abstract class GeneradorDatos {
	protected String nombre;
	protected double minValor;
	protected double maxValor;
	
	
	public void observarIntervalo(String nombre, double min, double max) {	
		this.nombre 	= nombre;
		minValor 		= min;
		maxValor 		= max;	
	}
	
	protected boolean verificarNuevaMedida(double medida) {
		return (medida >= minValor) && (medida <=maxValor);
	}
	
	public abstract DatoSensor generarNuevaMedida();
}
