package sujetos;

import granjaApp.RDGuardian;
import observadores.IObservador;
import observadores.TEMPOperario;
import simuladorSensores.DatoSensor;
import common.Sensor;

public class ReceptorInfoSensorTEMP_EXT extends ReceptorInfoSensoresConcretoPadre{
	public static double antTempExterior;
	private DatoSensor ultimoDatoProblema;
	public ReceptorInfoSensorTEMP_EXT() {
		super();
	}
	protected boolean verificarRealDecreto(Sensor sensor, DatoSensor datoSensor) {
		return (RDGuardian.verificarTemperatura( datoSensor.getValor(),antTempExterior));
	}
	protected void notificarOperarioConcreto(IObservador iobservador) {
		((TEMPOperario)iobservador).actualizar(this);
	}
	protected void notificarOperarioConcreto() {
		//No sabemos por qu� da error si ni metemos esto, as� que lo dejamos aqu� porque no afecta a la implementaci�n del c�digo.
		
	}

}
