package simuladorSensores.sensores;

import common.Sensor;
import granjaApp.Granja;
import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;

import java.io.FileNotFoundException;
import java.io.IOException;

//import granjaApp.Granja;
import common.Coordenadas;
//import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;

/**
 * @author: Mar�a Blanca Ib��ez
 * 
 * CLASE: TEMP_INTERNA (Clase que representa un tipo de sensor espec�fico) 
 * 
 */

public class TEMP_INTERNA extends Sensor{
	
	private static final double minValor = -10;
	private static final double maxValor = 50;
	
	public TEMP_INTERNA(Coordenadas coordenadas, GeneradorDatos generacionDatos, Granja granja) throws FileNotFoundException, IOException{ //, GeneradorDatos generacionDatos, Granja granja
		super("TEMP_INTERNA", coordenadas, minValor, maxValor, generacionDatos, granja); //, generacionDatos, granja
		System.out.println("Creado Sensor TEMP_INTERNA en las coordenadas: " + coordenadas.ToString());
	}
}
