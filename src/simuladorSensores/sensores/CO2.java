package simuladorSensores.sensores;

import common.Sensor;
import granjaApp.Granja;
import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;

import java.io.FileNotFoundException;
import java.io.IOException;

//import granjaApp.Granja;
import common.Coordenadas;
//import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;

/**
 * @author: Mar�a Blanca Ib��ez
 * 
 * CLASE: CO2 (Clase que representa un tipo de sensor espec�fico) 
 * 
 */

public class CO2 extends Sensor{
	
	private static final double minValor = 0;
	private static final double maxValor = 40000;
	
	public CO2(Coordenadas coordenadas,GeneradorDatos generacionDatos, Granja granja) throws FileNotFoundException, IOException{ //, GeneradorDatos generacionDatos, Granja granja
		super("CO2", coordenadas, minValor, maxValor, generacionDatos, granja ); //, generacionDatos, granja
		System.out.println("Creado Sensor CO2 en las coordenadas: " + coordenadas.ToString());
	}

}
