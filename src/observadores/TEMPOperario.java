package observadores;

import java.util.ArrayList;

import simuladorSensores.DatoSensor;
import sujetos.ReceptorInfoSensores;

public class TEMPOperario extends OperarioGranja{

	public TEMPOperario(ArrayList<ReceptorInfoSensores> receptores, String name) {
		super(receptores);
		super.nombre=name;
	}
	protected boolean interesado() {
		return true;
	}
	protected void actuar(DatoSensor dato) {
		System.out.println(super.nombre+" resuelve problemas con sensor de temperatura");
	}
	public String getNombre() {
		return this.nombre;
	}
}
