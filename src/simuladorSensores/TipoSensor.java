package simuladorSensores;

/**
 * @author: Mar�a Blanca Ib��ez
 * 
 * CLASE: TipoSensor 
 * 
 */

public enum TipoSensor {
	CO2, NH3, TEMP_EXTERNA, TEMP_INTERNA, HUMEDAD
}
