package controller;

import javax.swing.JOptionPane;

import granjaApp.Granja;
import model.SensorModel;
import view.MenuAdministrador;
import view.Identificador;

public class ElimSensorControl {

	MenuAdministrador menuAdministrador;
	Identificador identificador;
	SensorModel model;
	Granja granja;

	public ElimSensorControl(SensorModel model, Identificador identificador,Granja granja) {
		this.identificador = identificador;
		this.model = model;
		this.granja=granja;
		initDelSensorControl();
	}

	private void initDelSensorControl() {
		identificador.btnConfirmar.addActionListener(e ->EliminarSens());
	}
	public void EliminarSens() {
		//leer el fichero
		//model.recover_sensBD(this.granja);
		
		//variables de las ventanas (window)
		String nameW =  identificador.name_del.getText();
		
		//revisar condiciones
		if (model.DelConditions(nameW) == true) {
			model.eliminarSensor(nameW);
			model.dump_sens();
			//mensaje de confirmación	
			JOptionPane.showMessageDialog(null, 
					"Sensor con nombre: "+nameW+ 
					"\nfue eliminado correctamente");
			
		}else {
			//mensaje de error
			JOptionPane.showMessageDialog(null, 
						"Sensor con nombre: "+nameW+ 
						"\nNO fue eliminado correctamente");
		}


	}
}