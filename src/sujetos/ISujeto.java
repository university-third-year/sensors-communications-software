package sujetos;

import observadores.OperarioGranja;

public interface ISujeto {
	public void inscribir (OperarioGranja observador);
	public void borrar (OperarioGranja observador);
	public void notificar();
}
