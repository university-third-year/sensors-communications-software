package simuladorSensores.sensores;

import common.Sensor;
import granjaApp.Granja;
import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;

import java.io.FileNotFoundException;
import java.io.IOException;

//import granjaApp.Granja;
import common.Coordenadas;
//import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;

/**
 * @author: Mar�a Blanca Ib��ez
 * 
 * CLASE: NH3 (Clase que representa un tipo de sensor espec�fico) 
 * 
 */

public class NH3 extends Sensor{
	private static final double minValor = 0;
    private static final double maxValor = 100;


	
	public NH3(Coordenadas coordenadas, GeneradorDatos generacionDatos, Granja granja) throws FileNotFoundException, IOException{ //, GeneradorDatos generacionDatos, Granja granja
		super("NH3",coordenadas,  minValor, maxValor, generacionDatos, granja); //, generacionDatos, granja
		System.out.println("Creado Sensor NH3 en las coordenadas: " + coordenadas.ToString());
	}
}
