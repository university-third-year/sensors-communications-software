package controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

import common.*;
import granjaApp.*;
import model.*;
import observadores.OperarioGranja;
import simuladorSensores.estrategiasGeneracionDatos.*;
import sujetos.Receptor;
import sujetos.ReceptorInfoSensores;
import view.*;


public class InscribirControl {
	InscripcionSensor inscripcionSensor;
	SensorModel model;
	UsuarioModel model_;
	Granja granja;
	Receptor receptor;///////////////////////
	
	public InscribirControl(SensorModel model, UsuarioModel model_,InscripcionSensor inscripcionSensor,Granja granja,Receptor receptor){
		this.model=model;
		this.model_=model_;
		this.inscripcionSensor=inscripcionSensor;
		this.granja=granja;
		this.receptor=receptor;
		initInscribirControl();
	}
	public InscribirControl() {}
	private void initInscribirControl() {
		inscripcionSensor.btnContinuar.addActionListener(e-> inscribir());
	}
	public void inscribir() {
		
		//ver a que sensor nos inscribimos
		String inscripSensor = inscripcionSensor.textInscSensor.getText();
		String inscripTipo = model.sensoresDB.get(inscripSensor).getTipoSensor();
		
		
		//ver que operario somos
		String inscripNombre = model.sensoresDB.get(inscripSensor).getNombre();
		
		//inscribir al operario en una Lista de receptorInfoSensores(verUsuarioModel) y en la base de datos de Operarios.
		if(model_.buscarOperario(model_.getCurrentName())) {
			for( HashMap.Entry<String, ArrayList<String>> entradaTabla: model_.OperariosBD.entrySet() ) {
				if(entradaTabla.getKey().equals(model_.getCurrentName())) {
					entradaTabla.getValue().add(inscripSensor);
				}
			} 
		}else {
			ArrayList<String> sensors=new ArrayList<String>();
			model_.OperariosBD.put(model_.getCurrentName(), sensors);
		}
		model_.inscripcionOperario(inscripTipo, inscripNombre,this.receptor);
		
		
		//////////////////////////////////////////////////////////////////////////////////////
		//recep.inscribir(operarioAinscribir); lo comento porque al crear el operario, ya le est�s inscribiendo en el ArrayList<ReceptorInfoSensores>(mirar constructor OperarioGranja)	
	}
}


