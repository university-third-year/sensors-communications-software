package common;

import javax.swing.JOptionPane;

public class Usuario {
	private String loginName;
	private String password;
	private char type;
	public Usuario(String loginName, String password, char type) {
		this.loginName=loginName;
		try {
			this.setPassword(password);
		}catch(IllegalArgumentException exc) {
			System.err.print("Error en el tama�o de la contrase�a, debe ser mayor que 0 y menos que 8, adem�s de empezar por una letra");
		}
		try {
			this.setType(type);
		}catch(IllegalArgumentException excp) {
			System.err.print("Error en el tipo de empleado");
		}
	}

	public Usuario() {
	}

	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password)throws IllegalArgumentException  {
		char firstChar = password.charAt(0);
		Character f = new Character(firstChar); 
		if(password.length()<=8||password.length()>0||f.isDigit(firstChar)==false) {
			this.password = password;
		}else {
			throw new IllegalArgumentException();
		}
	}
	public char getType() {
		return type;
	}
	public void setType(char type) throws IllegalArgumentException {
		if(type=='a'||type=='o') {
			this.type = type;
		}else {
			throw new IllegalArgumentException();
		}
	}
	public String toString() {
		return loginName+"|"+ password+"|"+type;
	}


}

