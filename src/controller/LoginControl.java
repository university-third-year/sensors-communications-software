package controller;

import view.Interfazlogin;

import javax.swing.JOptionPane;

import granjaApp.Granja;
import model.SensorModel;
import model.UsuarioModel;
import sujetos.*;
public class LoginControl{
	
	Interfazlogin View;
	UsuarioModel model;
	SensorModel model_;
	Granja granja;
	Receptor receptor;
	
	public LoginControl(Interfazlogin view,UsuarioModel model,SensorModel model_,Granja granja,Receptor receptor) {
		this.View=view;
		this.model=model;
		this.model_=model_;
		this.granja=granja;
		this.receptor=receptor;
		initLoginControl();
	}
	
	public LoginControl() {
	}
	
	private void initLoginControl() {
		View.btnContinuar.addActionListener(e-> confirmacion());
	}
	
	@SuppressWarnings("deprecation")
	public void confirmacion() {
		model.recoverBD();
		String textUsuario = View.textUsuario.getText();
		String passwordContrase�a = View.passwordContrase�a.getText();
		char t= model.searchUsuario(textUsuario,passwordContrase�a);
		if(t=='a') {
			common.GlobalInstances.frameInterfazLogin.setVisible(false);
			common.GlobalInstances.frameMenuAdministrador.setVisible(true);
			model_.recover_sensBD(this.granja);

		}else if(t=='o'){
			common.GlobalInstances.frameInterfazLogin.setVisible(false);
			common.GlobalInstances.frameMenuOperador.setVisible(true);
			//con la siguiente funci�n, nos quedamos con qu� usuario est� usando la aplicaci�n, y recuperamos la base de datos de los operarios y sus suscripciones
			model.setCurrentOper(textUsuario,this.receptor);
			model_.recover_sensBD(this.granja);

		}else {
			common.GlobalInstances.frameInterfazLogin.setVisible(true);
			JOptionPane.showMessageDialog(null, "Usuario no encontrado o Contrase�a incorrecta");
		}
		
		View.textUsuario.setText("");
		View.passwordContrase�a.setText("");

	}

}
