package observadores;

import sujetos.ReceptorInfoSensores;

public interface IObservador {
	public void actualizar(ReceptorInfoSensores receptor);
}
