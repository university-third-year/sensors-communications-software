package sujetos;
import java.util.*;

import observadores.OperarioGranja;
import simuladorSensores.DatoSensor;
public abstract class ReceptorInfoSensores implements ISujeto{
	private ArrayList<OperarioGranja> suscriptores; 
	ReceptorInfoSensores() {
		
	}
	
	//OJO, ESTA ES LA FUNCION QUE NOS DA ERROR SIEMPRE 
	public void inscribir(OperarioGranja operarioGranja) {
		suscriptores.add(operarioGranja);
	}
	public void borrar(OperarioGranja operarioGranja) {
		
		suscriptores.remove(operarioGranja);
	}
	public void notificar() {
		for(int i=0;i<suscriptores.size();i++) {
			OperarioGranja obs=(OperarioGranja) suscriptores.get(i);
			obs.actualizar(this);
		}
	}
	protected abstract void notificarOperarioConcreto();
	public abstract DatoSensor getDatoSensor();
}
