package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

//import controller.CeseControl;
import model.UsuarioModel;
import common.GlobalInstances;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MenuAdministrador extends JFrame {
	
	public JButton btnAddSensor;
	public JButton btnDelSensor;
	GlobalInstances GI = GlobalInstances.getInstance();

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MenuAdministrador() {
		setTitle("Men\u00FA Administrador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JButton btnAddSensor = new JButton("A\u00F1adir Sensor");
		btnAddSensor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GI.frameMenuAdministrador.setVisible(false);
				GI.frameTipo.setVisible(true);
			}
		});

		JButton btnDelSensor = new JButton("Eliminar Sensor");
		btnDelSensor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GI.frameMenuAdministrador.setVisible(false);
				GI.frameIdentificador.setVisible(true);
			}
		});

		JButton btnLogOut = new JButton("LogOut");
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GI.frameConfirmPopUp.setVisible(true);
			}
		});

		JButton btnCesarUsuario = new JButton("Cese Usuario");
		btnCesarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GI.frameMenuAdministrador.setVisible(false);
				GI.frameInterfazCese.setVisible(true);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGap(29)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(btnDelSensor, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnAddSensor, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE))
						.addGap(72)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnCesarUsuario, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnLogOut, GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE))
						.addContainerGap(46, Short.MAX_VALUE))
				);
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGap(54)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnLogOut, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnAddSensor, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
						.addGap(27)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnCesarUsuario, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnDelSensor, GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE))
						.addContainerGap(65, Short.MAX_VALUE))
				);
		contentPane.setLayout(gl_contentPane);
	}

}
