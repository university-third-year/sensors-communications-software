package model;

import common.Coordenadas;
import common.Sensor;
import granjaApp.Granja;
import simuladorSensores.estrategiasGeneracionDatos.GeneracionLectura;
import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;
import simuladorSensores.sensores.CO2;
import simuladorSensores.sensores.HUMEDAD;
import simuladorSensores.sensores.NH3;
import simuladorSensores.sensores.TEMP_EXTERNA;
import simuladorSensores.sensores.TEMP_INTERNA;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class SensorModel {
	public Map<String, Sensor> sensoresDB;

	int TEcnt = 0;
	int TIcnt = 0;
	int NH3cnt = 0;
	int HUMcnt = 0;
	int CO2cnt = 0;
	
	String database = "sensoresBasic.txt";

	public SensorModel() {
		sensoresDB = new HashMap<String, Sensor>(); //key(name), Valores
	}

	//a�adimos en HashMap
	public int aniadirSensor(String tipo, Coordenadas coordenadas, GeneradorDatos generacionDatos, Granja granja) throws FileNotFoundException, IOException { 
		Sensor sensor = null;
		if (tipo.equals("TEMP_EXTERNA")) {
			sensor = new TEMP_EXTERNA(coordenadas,generacionDatos, granja);
		}
		if (tipo.equals("TEMP_INTERNA")) {
			sensor = new TEMP_INTERNA(coordenadas,generacionDatos, granja);
		}
		if (tipo.equals("NH3")) {
			sensor = new NH3(coordenadas,generacionDatos, granja);
		}
		if (tipo.equals("HUMEDAD")){
			sensor = new HUMEDAD(coordenadas,generacionDatos, granja);
		}
		if (tipo.equals("CO2")) {
			sensor = new CO2(coordenadas,generacionDatos, granja);
		}

		String name = sensor.getNombre();
		//System.out.println("ID del nuevo sensor: "+id);
		sensoresDB.put(name, sensor);	
		return sensor.getId();
	}

	public void eliminarSensor(String name) {
		sensoresDB.remove(name);
	}

	public boolean AddConditions(String tipo, int x, int y) {
		
	  //contar todos los sensores que hay de cada tipo
		TEcnt = 0;
		TIcnt = 0;
		NH3cnt = 0;
		HUMcnt = 0;
		CO2cnt = 0;
		
		for( HashMap.Entry<String, Sensor> entradaTabla: sensoresDB.entrySet() ) {
			String tmptipo = (String) entradaTabla.getValue().getTipoSensor();
			//System.out.println("tmptipo: "+ tmptipo);
			if (tmptipo.equals("TEMP_EXTERNA")) TEcnt++;
			if (tmptipo.equals("TEMP_INTERNA")) TIcnt++;
			if (tmptipo.equals("NH3")) NH3cnt++;
			if (tmptipo.equals("HUMEDAD")) HUMcnt++;
			if (tmptipo.equals("CO2")) CO2cnt++;
		}
		/*
		System.out.println("#TempExt:  "+TEcnt+"\n");
		System.out.println("#TempInt:  "+TIcnt+"\n");
		System.out.println("#Amoniaco: "+NH3cnt+"\n");
		System.out.println("#Humedad:  "+HUMcnt+"\n");
		System.out.println("#CO2:      "+CO2cnt+"\n");
		*/
		
		//condiciones para a�adir
		if (tipo.equals("TEMP_INTERNA")) {
			return true;
		}

		if (tipo.equals("TEMP_EXTERNA")) {
			if (TIcnt > TEcnt+1) {
				return true;
			}else {
				return false;
			}
		}

		if (tipo.equals("HUMEDAD")) {
			return true;
		}

		if (tipo.equals("NH3")) {
			if((HUMcnt > NH3cnt + CO2cnt + 1) && (CO2cnt > 2*NH3cnt + 1)) {
				return true;
			}else {
				return false;
			}
		}

		if (tipo.equals("CO2")) {
			if(HUMcnt > NH3cnt + CO2cnt + 1) {
				return true;
			}else {
				return false;
			}
		}
		return false;
	}

	public boolean DelConditions(String name) {
		TEcnt = 0;
		TIcnt = 0;
		NH3cnt = 0;
		HUMcnt = 0;
		CO2cnt = 0;
		for( HashMap.Entry<String, Sensor> entradaTabla: sensoresDB.entrySet() ) {
			String tmptipo = entradaTabla.getValue().getTipoSensor(); 

			if (tmptipo.equals("TEMP_EXTERNA")) TEcnt++;
			if (tmptipo.equals("TEMP_INTERNA")) TIcnt++;
			if (tmptipo.equals("NH3")) NH3cnt++;
			if (tmptipo.equals("HUMEDAD")) HUMcnt++;
			if (tmptipo.equals("CO2")) CO2cnt++;
		}
		
		/*
		System.out.println("#TempExt:  "+TEcnt+"\n");
		System.out.println("#TempInt:  "+TIcnt+"\n");
		System.out.println("#Amoniaco: "+NH3cnt+"\n");
		System.out.println("#Humedad:  "+HUMcnt+"\n");
		System.out.println("#CO2:      "+CO2cnt+"\n");
		*/

		String tipo = sensoresDB.get(name).getTipoSensor();

		//condiciones para eliminar
		if (tipo.equals("TEMP_INTERNA")) {
			if(TIcnt-1 > TEcnt) {
				return true;
			}else {
				return false;
			}
		}

		if (tipo.equals("TEMP_EXTERNA")) {
			return true;
		}

		if (tipo.equals("HUMEDAD")) {
			if(HUMcnt -1 > NH3cnt + CO2cnt) {
				return true;
			}else {
				return false;
			}
		}

		if (tipo.equals("NH3")) {
			return true;
		}

		if (tipo.equals("CO2")) {
			if(CO2cnt -1 > 2*NH3cnt) {
				return true;
			}else {
				return false;
			}
		}
		return false;
	}
	
	
	public void recover_sensBD(Granja granja) {
		boolean encontrado = false;
		
		try (FileReader fr = new FileReader(database);
		Scanner scanner = new Scanner(fr)) {
			while (scanner.hasNext()) {
				if(scanner.hasNext()){
					String data = scanner.nextLine();
					String[] pieces = data.split("\t");
					String id = pieces[0];
					//System.out.println("recover0: "+pieces[0]);
					String tipo = pieces[1];
					//System.out.println("recover1: "+pieces[1]);
					String coorX = pieces[2];
					//System.out.println("recover2: "+pieces[2]);
					String coorY = pieces[3];
					//System.out.println("recover3: "+pieces[3]);
					//System.out.println("---------------------");
					
					for( HashMap.Entry<String, Sensor> entradaTabla: sensoresDB.entrySet() ) {
						int x = entradaTabla.getValue().getCoordenadas().getX();
						int y = entradaTabla.getValue().getCoordenadas().getY();
						
						if(Integer.parseInt(coorX) == x && Integer.parseInt(coorY) == y) {
							encontrado = true;
						}
					}
					if (encontrado == false) {
						String file = "valores_"+tipo+"_"+id+".txt";
						GeneradorDatos gen = new GeneracionLectura(file);
						aniadirSensor(tipo, new Coordenadas(Integer.parseInt(coorX), Integer.parseInt(coorY)),gen,granja);
					} 
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void dump_sens() {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter(database);
			pw = new PrintWriter(fichero);
			
			for(Map.Entry<String, Sensor> entradaTabla: sensoresDB.entrySet()) { 
		        int id   = entradaTabla.getValue().getId(); 
		        String tipo = entradaTabla.getValue().getTipoSensor();
		        int x = entradaTabla.getValue().getCoordenadas().getX();
		        int y = entradaTabla.getValue().getCoordenadas().getY();
		        
				pw.println(id+ "\t" +tipo+"\t"+x+"\t"+y);
			}
		}catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(null != fichero) {fichero.close();}
			}catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	
	}

public Map<String, Sensor> getsensoresDB(){
	return sensoresDB;
}

public String ToString() {
	String s = "\nTabla actual de Sensores: <TipoSensor>_<Identificacion>_<CoordenadaX>_<CoordenadaY>"
			+ " \n===== ====== == ========  =========================================================\n";

	for(Map.Entry<String, Sensor> entradaTabla:sensoresDB.entrySet()){    
		String clave       = entradaTabla.getKey();  
		Sensor sensor   = entradaTabla.getValue();  

		s += sensor.toString() + "\n";
	} 	
	return s;
}

}
