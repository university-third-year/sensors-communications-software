package controller;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Map;
import javax.swing.JButton;

import common.Usuario;
import granjaApp.Granja;
import view.confirmPopUp;
import model.UsuarioModel;;
public class LogOutControl {
	confirmPopUp View;
	UsuarioModel model;
	public LogOutControl(confirmPopUp view,UsuarioModel model,Granja granja) {
		this.View=view;
		this.model=model;
		initUsuarioControl();
	}
	private void initUsuarioControl() {
		View.btnSi.addActionListener(e ->GuardarBD());
	}
	public void GuardarBD() {
		model.dump();
	}
}
