package simuladorSensores.estrategiasGeneracionDatos;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import simuladorSensores.DatoSensor;

/**
 * @author: Mar�a Blanca Ib��ez
 * 
 * CLASE: GeneracionLectura (Representa una estrategia de generaci�n de medidas de un sensor 
 * 		necesaria para probar la correctitud del programa)
 * 
 * FUNCIONALIDAD QUE LA DISTINGUE:
 *       Genera nueva medida a partir de la lectura de datos a partir de un fichero.
 *       
 */

public class GeneracionLectura extends GeneradorDatos{
	
	private int pos = 0;
	private ArrayList<Double> datos = new ArrayList<Double>();
	
	public GeneracionLectura(String fichero) throws FileNotFoundException, IOException{
		
		try (FileReader fr = new FileReader(fichero);
                Scanner scanner = new Scanner(fr)) {
			scanner.useLocale(Locale.US);
			while (scanner.hasNext()) {
				if(scanner.hasNextDouble()){
					double d = scanner.nextDouble();
					datos.add(d);
				}
			}
		}
	}

	@Override
	public DatoSensor generarNuevaMedida() {
		DatoSensor datoSensor = null;
		
		if (pos < datos.size()) {		
			datoSensor = new DatoSensor(nombre, datos.get(pos));	
			pos++;
		} 
			
		return datoSensor;
	}
	public String getNombre() {
		return nombre;
	}

}
