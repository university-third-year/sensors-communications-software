package simuladorSensores.sensores;

import java.io.FileNotFoundException;
import java.io.IOException;

//import granjaApp.Granja;
import common.Coordenadas;
//import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;
import common.Sensor;
import granjaApp.Granja;
import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;

/**
 * @author: Mar�a Blanca Ib��ez
 * 
 * CLASE: HUMEDAD (Clase que representa un tipo de sensor espec�fico) 
 * 
 */

public class HUMEDAD extends Sensor{
	
	private static final double minValor = 0;
	private static final double maxValor = 100;
	
	public HUMEDAD(Coordenadas coordenadas, GeneradorDatos generacionDatos, Granja granja) throws FileNotFoundException, IOException{ //, GeneradorDatos generacionDatos, Granja granja
		super("HUMEDAD", coordenadas, minValor, maxValor, generacionDatos, granja); //, generacionDatos, granja
		System.out.println("Creado Sensor HUMEDAD en las coordenadas: " + coordenadas.ToString());
	}
}