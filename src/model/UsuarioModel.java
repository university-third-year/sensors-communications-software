package model;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JPasswordField;
import javax.swing.JTextField;

import common.Usuario;
import observadores.*;
import simuladorSensores.sensores.CO2;
import simuladorSensores.sensores.HUMEDAD;
import simuladorSensores.sensores.NH3;
import simuladorSensores.sensores.TEMP_EXTERNA;
import simuladorSensores.sensores.TEMP_INTERNA;
import sujetos.*;

public class UsuarioModel {
	private HashMap<String,Usuario> UsuariosBD;
	public HashMap<String,ArrayList<String>> OperariosBD;
	private String JPasswordField;
	private String JTextField;
	char a = 'a';
	OperarioGranja currentOperario;
	String currentName;
	ArrayList<OperarioGranja> operarios;
	///////////////////////////////////////////////////////////////////////////////////

	//creamos las arraylists para poder trabajar sobre ellas y usarlas como argumentos en las funciones necesarias
	public ArrayList<ReceptorInfoSensores> humedad  = new ArrayList<ReceptorInfoSensores>();
	public ArrayList<ReceptorInfoSensores> co2  = new ArrayList<ReceptorInfoSensores>();
	public ArrayList<ReceptorInfoSensores> nh3  = new ArrayList<ReceptorInfoSensores>();
	public ArrayList<ReceptorInfoSensores> temp  = new ArrayList<ReceptorInfoSensores>();

	public UsuarioModel() {
		UsuariosBD=new HashMap<String,Usuario>();
		OperariosBD=new HashMap<String,ArrayList<String>>();
	}

	public HashMap<String, Usuario> getUsuariosBD() {
		return UsuariosBD;
	}
	public String getCurrentName() {
		return this.currentName;
	}
	public ArrayList<OperarioGranja> getOperarios(){
		return this.operarios;
	}

	public void addUsuario(Usuario usuario) {
		String name=usuario.getLoginName();
		UsuariosBD.put(name, usuario);
	}
	//A�ade un operario y los sensores a los que est� suscrito a OperariosBD.
	  
	public void addOperario(String oper,ArrayList<String> sensors) {
		OperariosBD.put(oper, sensors);
	}
	public void delUsuario(Usuario usuario) {
		String name=usuario.getLoginName();
		UsuariosBD.remove(name, usuario);
	}
	public void delOperario(String oper) {
		if(buscarOperario(oper)) OperariosBD.remove(oper);
	}
	public Usuario getUsuario(String UserName) {
		return UsuariosBD.get(UserName);
	}

	public boolean buscarUsuario(String name) {
		for( HashMap.Entry<String, Usuario> entradaTabla: UsuariosBD.entrySet() ) {
			if(entradaTabla.getValue().getLoginName().equals(name)) {
				return true;
			}
		}return false;
	}
	public boolean buscarOperario(String name) {
		for( HashMap.Entry<String, ArrayList<String>> entradaTabla: OperariosBD.entrySet() ) {
			if(entradaTabla.getKey().equals(name)) {
				return true;
			}
		}return false;
	}



	///////////////////////////////////////////////////////////////////////////////////////	
	//inscripcionOperario(String tipo, String nombreOpe,Receptor receptor):funci�n para inscribir a un operario
	/*String tipo: tipo de sensor al que se inscribe el usuario
	 String nombreOpe: nombre del operario
	 Receptor receptor: objeto que contiene el ReceptorInfoSensores que se debe de a�adir a su correspondiente ArrayList
	 */
	public OperarioGranja inscripcionOperario(String tipo, String nombreOpe,Receptor receptor) {

		OperarioGranja operario = null;

		if(tipo.equals("HUMEDAD")) {
			this.humedad.add(receptor.recephumedad);
			operario = new HUMEDADOperario(this.humedad, nombreOpe);
		} else if(tipo.equals("CO2")) {
			this.co2.add(receptor.recepCO2);
			operario = new CO2Operario(this.co2, nombreOpe);
		} else if(tipo.equals("NH3")) {
			this.nh3.add(receptor.recepNH3);
			operario = new NH3Operario(this.nh3, nombreOpe);
		} else if(tipo.equals("TEMP_INTERNA")) {
			this.temp.add(receptor.recepTEMPEXT);
			this.temp.add(receptor.recepTEMPINT);
			operario = new TEMPOperario(this.temp, nombreOpe);
		}
		return operario;
	}
	
	
	public boolean contarAdmin() {
		int count = 0;
		for( HashMap.Entry<String, Usuario> entradaTabla: UsuariosBD.entrySet() ) {
			if(entradaTabla.getValue().getType() == 'a') {
				count++; 
			}
		}

		if(count < 3) {
			return false;
		}else return true;
	}
	

	public void recoverBD() {
		try (FileReader fr = new FileReader("usuariosBasic.txt");
				Scanner scanner = new Scanner(fr)) {
			while (scanner.hasNext()) {
				if(scanner.hasNext()){
					String data = scanner.nextLine();
					String[] pieces = data.split("\t");
					String loginName = pieces[0];
					String password = pieces[1];
					char type= pieces[2].charAt(0);
					addUsuario(new Usuario(loginName, password, type));
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*
	 recoverBDinscritos(Receptor receptor): lee el fichero inscritos.txt, guardando e inscribiendo los operarios a sus respectivos sensores.
	 Receptor receptor: objeto que contiene el ReceptorInfoSensores que se debe de a�adir a su correspondiente ArrayList. Se pasar� como argumento a inscripcionOperario().
	 */
	public void recoverBDinscritos(Receptor receptor) {
		int i=0;
		String operName="";
		OperarioGranja oper;
		try (FileReader fr = new FileReader("inscritos.txt");
				Scanner scanner = new Scanner(fr)) {
			while (scanner.hasNext()) {
				ArrayList<String> sensors =new ArrayList<String>();
				if(scanner.hasNext()){
					String data = scanner.nextLine();
					String[] pieces = data.split("\t");
					for(int j=0;j<pieces.length;j++) {
						if(j==0) {
							operName= pieces[j];
						}else {
							String[] pieces2=pieces[j].split("_");
							String tipo=pieces2[0];
							String sensor=pieces[j];
							sensors.add(tipo);
						}
					}
					if(sensors.size()>0) {
						int flag=0;
						String prevType="";
					for(int j=0;j<sensors.size();j++) {
						if(flag==0 || !prevType.equals(sensors.get(j))) {
							oper=inscripcionOperario(sensors.get(j),operName,receptor);
							this.operarios.add(oper);
							flag++;
							if(operName.equals(this.currentName)) {
								this.currentOperario=oper;
							}
						}
						prevType=sensors.get(j);
					}
					if(!buscarOperario(operName))addOperario(operName,sensors);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*Dump(): genera dos ficheros, uno con la base de datos de los usuarios y su respectivas contrase�as y roles (usuarios_db.txt), y otro con los operarios y 
	  los sensores a los que cada uno est� inscrito.
	 */
	public void dump() {
		FileWriter fichero=null;
		PrintWriter pw=null;
		try {
			fichero=new FileWriter("usuarios_db.txt");
			pw=new PrintWriter(fichero);

			for(Map.Entry<String, Usuario> entradaTabla: UsuariosBD.entrySet()) {
				String identificaci�n= entradaTabla.getKey();
				Usuario usuario=entradaTabla.getValue();
				pw.println(usuario.getLoginName()+"\t"+usuario.getPassword()+"\t"+usuario.getType());
			}
		}catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(null != fichero) {fichero.close();}
			}catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		try {
			fichero=new FileWriter("inscritos.txt");
			pw=new PrintWriter(fichero);

			for(Map.Entry<String, ArrayList<String>> entradaTabla: OperariosBD.entrySet()) {
				String oper= entradaTabla.getKey();
				if(entradaTabla.getValue().size()==1)pw.println(oper+"\t"+entradaTabla.getValue().get(0));
				if(entradaTabla.getValue().size()==2)pw.println(oper+"\t"+entradaTabla.getValue().get(0)+"\t"+entradaTabla.getValue().get(1));
				if(entradaTabla.getValue().size()==3)pw.println(oper+"\t"+entradaTabla.getValue().get(0)+"\t"+entradaTabla.getValue().get(1)+"\t"+entradaTabla.getValue().get(2));
				if(entradaTabla.getValue().size()==4)pw.println(oper+"\t"+entradaTabla.getValue().get(0)+"\t"+entradaTabla.getValue().get(1)+"\t"+entradaTabla.getValue().get(2)+"\t"+entradaTabla.getValue().get(3));
				if(entradaTabla.getValue().size()==5)pw.println(oper+"\t"+entradaTabla.getValue().get(0)+"\t"+entradaTabla.getValue().get(1)+"\t"+entradaTabla.getValue().get(2)+"\t"+entradaTabla.getValue().get(3)+"\t"+entradaTabla.getValue().get(4));


			}
		}catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(null != fichero) {fichero.close();}
			}catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
	public char searchUsuario(String textUsuario, String passwordContrase�a) {
		Usuario us= new Usuario();
		for(Map.Entry<String, Usuario> entradaTabla: UsuariosBD.entrySet()) {
			String identificacion= entradaTabla.getKey();
			Usuario usuario=entradaTabla.getValue();
			if(textUsuario.equals(usuario.getLoginName())) {
				if( passwordContrase�a.equals(usuario.getPassword())) {
					us= usuario;
				}
			} 
		}
		return us.getType();
	}
	/*setCurrentOper(String currentUser,Receptor receptor): funci�n que establece el operario que est� trabajando en este instante con la app, y que a su vez llama a 
	  recoverBDinscritos.
	  String currentUser: sirve para establecer el usuario que usa la aplicaci�n
	 Receptor receptor: objeto que contiene el ReceptorInfoSensores que se debe de a�adir a su correspondiente ArrayList. Se pasar� como argumento a recoverBDinscritos().
	  */
	public void setCurrentOper(String currentUser,Receptor receptor) {
		this.currentName=currentUser;
		recoverBDinscritos(receptor);
	}
	public OperarioGranja getCurrentOper() {
		return this.currentOperario;
	}

	public Usuario checkUsuario(String name) {
		Usuario auxUser = new Usuario();
		auxUser = null;
		for (Map.Entry<String, Usuario> entradaTabla : UsuariosBD.entrySet()) {
			Usuario usuario = entradaTabla.getValue();

			if (name.equals(usuario.getLoginName())) {
				auxUser = usuario;
			}
		}
		return auxUser;
	}

}

