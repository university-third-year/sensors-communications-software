package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import common.GlobalInstances;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegistroUsuario extends JFrame {

	GlobalInstances GI = GlobalInstances.getInstance();

	private JPanel RegistroUsuario;
	public JTextField textFieldUsuario;
	public JPasswordField passwordField;
	private JLabel lblContrasenia;
	private JLabel lbl;
	public JTextField textFieldRol;
	public JButton btnContinuar;

	/**
	 * Create the frame.
	 */
	public RegistroUsuario() {
		setTitle("Registro Usuario");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		RegistroUsuario = new JPanel();
		RegistroUsuario.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(RegistroUsuario);
		
		JLabel lblNombre = new JLabel("Usuario");
		
		textFieldUsuario = new JTextField();
		textFieldUsuario.setColumns(10);
		
		passwordField = new JPasswordField();
		
		lblContrasenia = new JLabel("Contrase\u00F1a");
		
		lbl = new JLabel("ROL (Escribir 'a' para administrador, 'o' para operario)");
		lbl.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		btnContinuar = new JButton("Continuar");
		
		btnContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textFieldUsuario.setText("");
				passwordField.setText("");
				textFieldRol.setText("");
			}
			
		});
		
		
		textFieldRol = new JTextField();
		textFieldRol.setColumns(10);
		GroupLayout gl_RegistroUsuario = new GroupLayout(RegistroUsuario);
		gl_RegistroUsuario.setHorizontalGroup(
			gl_RegistroUsuario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_RegistroUsuario.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_RegistroUsuario.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnContinuar)
						.addGroup(gl_RegistroUsuario.createParallelGroup(Alignment.LEADING, false)
							.addGroup(Alignment.TRAILING, gl_RegistroUsuario.createSequentialGroup()
								.addComponent(lbl, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(textFieldRol, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_RegistroUsuario.createSequentialGroup()
								.addGroup(gl_RegistroUsuario.createParallelGroup(Alignment.LEADING)
									.addComponent(lblNombre, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
									.addComponent(lblContrasenia))
								.addGap(155)
								.addGroup(gl_RegistroUsuario.createParallelGroup(Alignment.LEADING, false)
									.addComponent(textFieldUsuario)
									.addComponent(passwordField, GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE)))))
					.addGap(8))
		);
		gl_RegistroUsuario.setVerticalGroup(
			gl_RegistroUsuario.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_RegistroUsuario.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_RegistroUsuario.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNombre)
						.addComponent(textFieldUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_RegistroUsuario.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblContrasenia)
						.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_RegistroUsuario.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbl, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
						.addComponent(textFieldRol, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
					.addComponent(btnContinuar)
					.addContainerGap())
		);
		RegistroUsuario.setLayout(gl_RegistroUsuario);
	}
}

