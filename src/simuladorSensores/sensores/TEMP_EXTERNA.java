package simuladorSensores.sensores;

import common.Sensor;
import granjaApp.Granja;
import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;

import java.io.FileNotFoundException;
import java.io.IOException;

//import granjaApp.Granja;
import common.Coordenadas;
//import simuladorSensores.estrategiasGeneracionDatos.GeneradorDatos;

/**
 * @author: Mar�a Blanca Ib��ez
 * 
 * CLASE: TEMP_EXTERNA (Clase que representa un tipo de sensor espec�fico) 
 * 
 */

public class TEMP_EXTERNA extends Sensor{
	
	private static final double minValor = -50;
	private static final double maxValor = 50;
	
	public TEMP_EXTERNA(Coordenadas coordenadas, GeneradorDatos generacionDatos, Granja granja) throws FileNotFoundException, IOException{ //, GeneradorDatos generacionDatos, Granja granja
		super("TEMP_EXTERNA", coordenadas, minValor, maxValor, generacionDatos, granja); //, generacionDatos, granja
		System.out.println("Creado Sensor TEMP_EXTERNA en las coordenadas: " + coordenadas.ToString());
	}
}
