package common;

public class Coordenadas {

	private int x;
	private int y;
	
	/**
	 * 
	 * @param x: coordenada x del sensor; no se verifica que la coordenada est� en el rango que le corresponde al sensor
	 * @param y: coordenada y del sensor; no se verifica que la coordenada est� en el rango que le corresponde al sensor
	 */
	public Coordenadas (int x, int y){	
		this.x = x;	
		this.y = y;
	}
	
	public String ToString() {
		return x + "_" + y;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public String toString() {
		return "<" + x + "," + y + ">";
	}
	

}
