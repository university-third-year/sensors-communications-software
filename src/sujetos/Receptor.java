/*
  Esta es una clase que creamos por conveniencia, ya que nos es m�s f�cil pasar un solo objeto Receptor como argumento, y luego elegir el tipo de receptor seg�n nos convega.
 */
 
package sujetos;
import sujetos.*;
public class Receptor {
	public ReceptorInfoSensorCO2 recepCO2;
	public ReceptorInfoSensorHUMEDAD recephumedad;
	public ReceptorInfoSensorNH3 recepNH3;
	public ReceptorInfoSensorTEMP_EXT recepTEMPEXT;
	public ReceptorInfoSensorTEMP_INT recepTEMPINT;
	public Receptor() {
		this.recepCO2=new ReceptorInfoSensorCO2();
		this.recephumedad=new ReceptorInfoSensorHUMEDAD();
		this.recepNH3=new ReceptorInfoSensorNH3();
		this.recepTEMPEXT=new ReceptorInfoSensorTEMP_EXT();
		this.recepTEMPINT=new ReceptorInfoSensorTEMP_INT();
	}

}
