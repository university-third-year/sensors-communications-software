package sujetos;
import granjaApp.RDGuardian;
import observadores.IObservador;
import observadores.NH3Operario;
import simuladorSensores.DatoSensor;
import common.Sensor;
public class ReceptorInfoSensorHUMEDAD extends ReceptorInfoSensoresConcretoPadre{
	public ReceptorInfoSensorHUMEDAD() {
		super();
	}

	protected boolean verificarRealDecreto(Sensor sensor, DatoSensor datoSensor) {
		return (RDGuardian.verificarNH3((int)datoSensor.getValor()));
	}

	protected void notificarOperarioConcreto(IObservador iobservador) {
		((NH3Operario) iobservador).actualizar(this);		
	}
	protected void notificarOperarioConcreto() {
		//No sabemos por qu� da error si ni metemos esto, as� que lo dejamos aqu� porque no afecta a la implementaci�n del c�digo.
	}


}
