package controller;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import common.Usuario;
import granjaApp.Granja;
import common.GlobalInstances;
import view.InterfazCese;
import view.RegistroUsuario;
import model.UsuarioModel;



//Cuando se cesa un usuario, hay que desinscribirle de todos los sensores de los que est� suscrito



public class CeseControl {
	InterfazCese view;
	UsuarioModel uModel;

	/*
	 * Creamos el constructor de la clase y uno vac�o para instanciar variables
	 * futuras
	 */
	public CeseControl() {;}	
	
	public CeseControl(InterfazCese view, UsuarioModel uModel,Granja granja) {
		this.view = view;
		this.uModel = uModel;
		initCeseControl();
	}
	
	private void initCeseControl() {
		view.btnCesarUsuario.addActionListener(e->cesar());
	}

	public void cesar() {
		
		//uModel.recoverBD();
		//guardamos nombre del usuario a cesar
		String usr = view.delUsuario.getText();
		
		//creamos un usuario con el nombre a borrar
		Usuario usuarioCesar = new Usuario();
		usuarioCesar = uModel.checkUsuario(usr);
		
		
		if (usuarioCesar != null) {
			uModel.delUsuario(usuarioCesar);
			//uModel.dump();
			JOptionPane.showMessageDialog(null, "Usuario con nombre: " 
			+ usr + "\nfue cesado correctamente");
			
		}else {
			JOptionPane.showMessageDialog(null, "Usuario con nombre: " 
					+ usr + "\nNO fue cesado.");
		}
	}
}
