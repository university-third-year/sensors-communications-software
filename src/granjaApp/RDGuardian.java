package granjaApp;

public final class RDGuardian {
	public final String nombre;
	
	RDGuardian(){
		nombre = "Real Decreto 692/2010, de 20 de mayo";
	}

	/*
	 * RD:: "La concentraci�n de di�xido de carbono (CO2) no supere las 3000 ppm"
	 */
	public static boolean verificarCO2(int ppmMedido){
		return ppmMedido <= 3000;
	}
	
	/*
	 *  RD:: "La concentraci�n de amon�aco (NH3) no sea superior a 20 ppm "
	 */
	public static boolean verificarNH3(int ppmMedido){
		return ppmMedido <= 20;
	}
	
	/*
	 *  RD:: "La temperatura interior no exceda de la temperatura exterior en m�s de 3� C 
	 *  cuando esta �ltima, medida a la sombra, supere los 30� C"
	 *  
	 *  Por convenci�n, valores de temperatura iguales a Double.MIN_VALUE 
	 *  significa que al stma acaba de comenzar a trabajar, por ello respuesta: true
	 */
	public static boolean verificarTemperatura(double tmpExterior, double tmpInterior ){
		if ((tmpExterior == Double.MIN_VALUE) || (tmpInterior == Double.MIN_VALUE)) 
			return true;
		 else
			 return !((tmpExterior > 30) & (tmpInterior > tmpExterior) & ((tmpInterior - tmpExterior) <= 3.0));
	}
	
	/*
	 *  RD:: "La humedad relativa media dentro del gallinero  no supere el 70%, 
	 *  cuando la temperatura exterior sea inferior a 10� C"
	 *  
	 * Por convenci�n, valores de temperatura o de humedad iguales a Double.MIN_VALUE 
	 * significa que al stma acaba de comenzar a trabajar, por ello respuesta: true
	*/
	public static boolean verificarHumedad(double tmpExterior, double humedad){
		if ((tmpExterior == Double.MIN_VALUE) || (humedad == Double.MIN_VALUE)) 
			return true;
		else
			return !((tmpExterior < 10.0) & (humedad <= 70.0));
	}
}
