package controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

import common.*;
import granjaApp.*;
import model.*;
import observadores.OperarioGranja;
import simuladorSensores.estrategiasGeneracionDatos.*;
import sujetos.ReceptorInfoSensores;
import view.*;


public class DesinscribirControl {
	DesinscripcionSensor desinscripcionSensor;
	SensorModel model;
	UsuarioModel model_;
	Granja granja;

	
	public DesinscribirControl(SensorModel model, UsuarioModel model_,DesinscripcionSensor desinscripcionSensor,Granja granja) {
		this.model=model;
		this.model_=model_;
		this.desinscripcionSensor=desinscripcionSensor;
		this.granja=granja;
		initDesinscribirControl();
	}
	
	public DesinscribirControl() {}
	private void initDesinscribirControl() {
		desinscripcionSensor.btnContinuar.addActionListener(e-> desinscribir());
	}
	public void desinscribir() {
		//ver de que sensor nos desinscribimos
		String desinscripSensor = desinscripcionSensor.textDesinsSensor.getText();
		String desinscripTipo = this.model.sensoresDB.get(desinscripSensor).getTipoSensor();
		//eliminar ese sensor del arrayList de sensores para ese operario en OperariosBD
		if(model_.OperariosBD.containsKey(model_.getCurrentName())) {
			for( HashMap.Entry<String, ArrayList<String>> entradaTabla: model_.OperariosBD.entrySet() ) {
				if(entradaTabla.getKey().equals(model_.getCurrentName())) {
					entradaTabla.getValue().remove(desinscripSensor);
				}
				if(entradaTabla.getValue().size()==0) model_.OperariosBD.remove(model_.getCurrentName());
			}
		}else {
			System.out.println("No est�s suscrito a ning�n sensor");
		}
		//Dependiendo del tipo, vamos a eliminar al operario que actualmente est� usando la aplicaci�n de su respectiva lista de ReceptorInfoSensores
		switch(desinscripTipo) {
		case "CO2":
			for(int i=0;i<model_.co2.size();i++) {
				model_.co2.get(i).borrar(model_.getCurrentOper());
			}
		case "HUMEDAD":
			for(int i=0;i<model_.humedad.size();i++) {
				model_.co2.get(i).borrar(model_.getCurrentOper());
			}
		case "NH3":
			for(int i=0;i<model_.nh3.size();i++) {
				model_.co2.get(i).borrar(model_.getCurrentOper());
			}
		case "TEMP_EXTERNA":
			for(int i=0;i<model_.temp.size();i++) {
				model_.co2.get(i).borrar(model_.getCurrentOper());
			}
		case "TEMP_INTERNA":
			for(int i=0;i<model_.temp.size();i++) {
				model_.co2.get(i).borrar(model_.getCurrentOper());
			}
		}
	}
	
	
	
	
	
	
	
	
}
