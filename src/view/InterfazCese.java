package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.CeseControl;
import common.GlobalInstances;
import model.UsuarioModel;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;

public class InterfazCese extends JFrame {

	private JPanel contentPane;
	public JTextField delUsuario;
	public JButton btnCesarUsuario;
	GlobalInstances GI = GlobalInstances.getInstance();
	
	public InterfazCese() {
		setTitle("Men\u00FA Administrador");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		Font font = new Font("Tahoma", Font.PLAIN, 20);
		
		btnCesarUsuario = new JButton("Cesar Usuario");
		btnCesarUsuario.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				GI.frameMenuAdministrador.setVisible(true);
				GI.frameInterfazCese.setVisible(false);
				delUsuario.setText("");
			}
		});
		btnCesarUsuario.setFont(font);
		delUsuario = new JTextField();
		delUsuario.setColumns(10);
		
		
		JLabel labelCesar= new JLabel("Introduce el nombre de usuario a cesar");
		
		labelCesar.setFont(font);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(32)
							.addComponent(labelCesar))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(124)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(delUsuario, Alignment.LEADING)
								.addComponent(btnCesarUsuario, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
					.addContainerGap(44, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(56)
					.addComponent(labelCesar)
					.addGap(18)
					.addComponent(delUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
					.addComponent(btnCesarUsuario, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
					.addGap(45))
		);
		contentPane.setLayout(gl_contentPane);
	
	}

	/*@SuppressWarnings("unused")
	private class botonPulsado implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			 String name = textField.getText();
			 
			
		}

	}*/
	public  String getUserName() {
		return delUsuario.getText();
	}
	
}
