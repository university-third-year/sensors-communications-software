package sujetos;

import common.Sensor;
import observadores.IObservador;
import simuladorSensores.DatoSensor;
import simuladorSensores.sensores.*;
public abstract class ReceptorInfoSensoresConcretoPadre extends ReceptorInfoSensores{
	private DatoSensor ultimoDatoProblema;
	ReceptorInfoSensoresConcretoPadre() {
		super();
	}
	public void recibirDato(Sensor sensor, DatoSensor datoSensor) {
		System.out.println("Recibi: "+datoSensor.toString());
		if(!verificarRealDecreto(sensor,datoSensor)) {
			System.out.println(" **no** cumple con el RD");
			setDatoSensor(datoSensor);
			notificar();
		} else {
			System.out.println(" y cumple con el RD");
		}
	}
	protected void setDatoSensor(DatoSensor datoSensor) {
		ultimoDatoProblema=ultimoDatoProblema;
	}
	public DatoSensor getDatoSensor() {
		return ultimoDatoProblema;
	}
	protected abstract boolean verificarRealDecreto(Sensor sensor,DatoSensor datoSensor);
	protected abstract void notificarOperarioConcreto(IObservador iobservador);
}
