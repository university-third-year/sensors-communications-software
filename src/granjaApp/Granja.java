package granjaApp;

import simuladorSensores.DatoSensor;
import simuladorSensores.sensores.*;
import common.*;
import sujetos.ReceptorInfoSensorCO2;
import sujetos.ReceptorInfoSensorHUMEDAD;
import sujetos.ReceptorInfoSensorNH3;
import sujetos.ReceptorInfoSensorTEMP_EXT;
import sujetos.ReceptorInfoSensorTEMP_INT;

public class Granja {
	
	// Quienes recibiran datos de los sensores
	private ReceptorInfoSensorNH3 receptorInfoSensorNH3;
	private ReceptorInfoSensorCO2 receptorInfoSensorCO2;
	private ReceptorInfoSensorTEMP_EXT receptorInfoSensorTEMP_EXT;
	private ReceptorInfoSensorTEMP_INT receptorInfoSensorTEMP_INT;
	private ReceptorInfoSensorHUMEDAD receptorInfoSensorHUMEDAD;
			
	Granja(){
	}
	
	public ReceptorInfoSensorNH3 getReceptorInfoSensorNH3() {
		return receptorInfoSensorNH3;
	}

	public void setReceptorInfoSensorNH3(ReceptorInfoSensorNH3 receptorInfoSensorNH3) {
		this.receptorInfoSensorNH3 = receptorInfoSensorNH3;
	}
	
	public ReceptorInfoSensorHUMEDAD getReceptorInfoSensorHUMEDAD() {
		return receptorInfoSensorHUMEDAD;
	}

	public void setReceptorInfoSensorHUMEDAD(ReceptorInfoSensorHUMEDAD receptorInfoSensorHUMEDAD) {
		this.receptorInfoSensorHUMEDAD = receptorInfoSensorHUMEDAD;
	}

	public ReceptorInfoSensorCO2 getReceptorInfoSensorCO2() {
		return receptorInfoSensorCO2;
	}

	public void setReceptorInfoSensorCO2(ReceptorInfoSensorCO2 receptorInfoSensorCO2) {
		this.receptorInfoSensorCO2 = receptorInfoSensorCO2;
	}

	public ReceptorInfoSensorTEMP_EXT getReceptorInfoSensorTEMP_EXT() {
		return receptorInfoSensorTEMP_EXT;
	}

	public void setReceptorInfoSensorTEMP_EXT(ReceptorInfoSensorTEMP_EXT receptorInfoSensorTEMP_EXT) {
		this.receptorInfoSensorTEMP_EXT = receptorInfoSensorTEMP_EXT;
	}

	public ReceptorInfoSensorTEMP_INT getReceptorInfoSensorTEMP_INT() {
		return receptorInfoSensorTEMP_INT;
	}

	public void setReceptorInfoSensorTEMP_INT(ReceptorInfoSensorTEMP_INT receptorInfoSensorTEMP_INT) {
		this.receptorInfoSensorTEMP_INT = receptorInfoSensorTEMP_INT;
	}

	public void recibirDato(Sensor sensor, DatoSensor dato){
		
		if (sensor instanceof CO2) {
			receptorInfoSensorCO2.recibirDato(sensor, dato);
		} else {
			if (sensor instanceof NH3) {
				receptorInfoSensorNH3.recibirDato(sensor, dato);
			} else {
				if (sensor instanceof TEMP_EXTERNA) {
					receptorInfoSensorTEMP_EXT.recibirDato(sensor, dato);
				} else {
					if (sensor instanceof TEMP_INTERNA) {	
						receptorInfoSensorTEMP_INT.recibirDato(sensor, dato);
					} else {
						if(sensor instanceof HUMEDAD) {
							receptorInfoSensorHUMEDAD.recibirDato(sensor, dato);
						}
					}
				}
			}
		}
	}
}
