package observadores;

import java.util.ArrayList;

import simuladorSensores.DatoSensor;
import sujetos.ReceptorInfoSensores;

public class NH3Operario extends OperarioGranja{

	public NH3Operario(ArrayList<ReceptorInfoSensores> receptores,String nombre) {
		super(receptores);
		super.nombre=nombre;
	}
	protected boolean interesado() {
		return true;
	}
	protected void actuar(DatoSensor dato) {
		System.out.println(super.nombre+" resuelve problemas con sensor de NH3");
	}
	public String getNombre() {
		return this.nombre;
	}
}
